FROM golang:1.20-alpine as builder
ENV GOPROXY=https://goproxy.cn,direct
ENV CGO_ENABLED=0
WORKDIR /build
ADD . /build
RUN sleep 12m && go build -o hello main.go

FROM alpine:latest
ENV TZ=Asia/Shanghai
WORKDIR /app
COPY --from=builder /build/hello /app/
EXPOSE 80
CMD [ "/app/hello" ]

