package main

import (
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	e := echo.New()
	e.Debug = true

	e.Use(middleware.Logger())
	e.Use(middleware.Gzip())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	e.GET("/", Hello)
	e.GET("/healthz", Healthz)
	e.GET("/sleep", Sleep)

	e.Logger.Fatal(e.Start(":80"))
}

func Hello(c echo.Context) error {
	return c.JSON(http.StatusOK, "hello world")
}

func Healthz(c echo.Context) error {
	return c.String(http.StatusOK, "ok")
}

func Sleep(c echo.Context) error {
	var (
		duration = 150 * time.Millisecond
	)
	err := echo.QueryParamsBinder(c).
		Duration("duration", &duration).
		BindError()
	if err != nil {
		return err
	}

	c.Logger().Infof("sleeping for %v", duration)
	time.Sleep(duration)

	return c.String(http.StatusOK, "ok")
}
